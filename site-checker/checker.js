'use strict';

const AWS = require('aws-sdk');
const SSM = new AWS.SSM();
const FS = require('fs');
var url = require('url');

exports.handler = function(event, context, callback) {

  var alias = context.invokedFunctionArn.split(':').pop();
  console.log('Alias/version: '+ alias);

  var raw_data = FS.readFileSync('config.json');
  var config = JSON.parse(raw_data);
  var namespace = null;

  if (config[alias]) {
    namespace = config[alias];
  } else {
    namespace = config.default;
  }

  console.log("Namespace: "+ namespace);

  SSM.getParameter({
    Name: 'site-checker-url-'+ namespace,
    WithDecryption: false
  }, (err, data) => {
    console.log("Err: "+ err);
    console.log("Data: "+ data);
    if (err) {
      console.log('[ERROR] - ' + err.message);
      callback(err);
    } else {
      if (data.Parameter !== undefined) {
        var target = data.Parameter.Value;
        console.log("Target: "+ target);
        var urlObject = url.parse(target);
        var mod = require(
          urlObject.protocol.substring(0, urlObject.protocol.length - 1)
        );
        console.log('[INFO] - Checking ' + target);
        var req = mod.request(urlObject, function(res) {
          res.setEncoding('utf8');
          res.on('data', function(chunk) {
            console.log('[INFO] - Read body chunk');
          });
          res.on('end', function() {
            console.log('[INFO] - Response end');
            callback();
          });
        });

        req.on('error', function(e) {
          console.log('[ERROR] - ' + e.message);
          callback(e);
        });
        req.end();
      }
    }
  });
};
